#!/usr/bin/python

import sys
import urllib 
import boto3
import requests
import tzlocal
import getpass
import ConfigParser
import base64
import logging
import time
import xml.etree.ElementTree as ET
import re
from bs4 import BeautifulSoup
from os.path import expanduser
from urlparse import urlparse, urlunparse
import json

#Disabling SSL Warnings - SNIMissingWarning and InsecurePlatformWarning 
requests.packages.urllib3.disable_warnings()

##########################################################################
# Variables

# region: The default AWS region that this script will connect
# to for all API calls
region = 'us-east-1'

# output format: The AWS CLI output format that will be configured in the
# saml profile (affects subsequent CLI calls)
outputformat = 'json'

# awsconfigfile: The file where this script will store the temp
# credentials under the saml profile
awsconfigfile = '/.aws/credentials'

# SSL certificate verification: Whether or not strict certificate
# verification is done, False should only be used for dev/test
sslverification = True
#sslverification = False

#idpentryurl: The initial url that starts the authentication process.
#idpentryurl = 'https://shibbolethqa.es.its.nyu.edu/idp/profile/SAML2/Unsolicited/SSO?providerId=urn:amazon:webservices'
idpentryurl = 'https://shibboleth.nyu.edu/idp/profile/SAML2/Unsolicited/SSO?providerId=urn:amazon:webservices'


# Set access token expiration time
# in second
tokenexpiration = 3600

# Duo timeout limit
#duotimeout = 20

# Uncomment to enable low level debugging
#logging.basicConfig(level=logging.DEBUG)

##########################################################################

# Get the federated credentials from the user
print "\n*****************************************************************"
print "**                        NYU Login                            **"
print "*****************************************************************"
print '\n'
print "Username:",
username = raw_input()
password = getpass.getpass()
print "\n"
print '            NYU Multi-factor Authentication'
print '            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
#print '\nPlease enter a passcode or select one of the following authentication method:\n'
print '* Enter "push" = push notification to your smartphone'
print '* Enter "call" = a call to your first registered phone'
print '* Enter passcode from your registered token generator'
print '\nPlease select an authentication method:',
authmethod = raw_input()
print ''

#Detect if string has number
_digits = re.compile('\d')
def contains_digits(d):
    return bool(_digits.search(d))

duo = contains_digits(authmethod)

# Initiate session handler
session = requests.Session()

# Programmatically get the SAML assertion
# Opens the initial IdP url and follows all of the HTTP302 redirects, and
# gets the resulting login page
formresponse = session.get(idpentryurl, verify=sslverification)
# Capture the idpauthformsubmiturl, which is the final url after all the 302s
idpauthformsubmiturl = formresponse.url

# Parse the response and extract all the necessary values
# in order to build a dictionary of all of the form values the IdP expects
formsoup = BeautifulSoup(formresponse.text.decode('utf8'), "html5lib")
payload = {}

for inputtag in formsoup.find_all(re.compile('(INPUT|input)')):
    name = inputtag.get('name','')
    value = inputtag.get('value','')
    if "user" in name.lower():
        #Make an educated guess that this is the right field for the username
        payload[name] = username
    elif "email" in name.lower():
        #Some IdPs also label the username field as 'email'
        payload[name] = username
    elif "pass" in name.lower():
        #Make an educated guess that this is the right field for the password
        payload[name] = password
    else:
        #Simply populate the parameter with the existing value (picks up hidden fields in the login form)
        payload[name] = value

payload['_eventId_proceed'] =  "Login"

# Debug the parameter payload if needed
# Use with caution since this will print sensitive output to the screen

# Some IdPs don't explicitly set a form action, but if one is set we should
# build the idpauthformsubmiturl by combining the scheme and hostname 
# from the entry url with the form action target
# If the action tag doesn't exist, we just stick with the 
# idpauthformsubmiturl above

for inputtag in formsoup.find_all(re.compile('(FORM|form)')):
    action = inputtag.get('action')
    if action:
        parsedurl = urlparse(idpentryurl)
        idpauthformsubmiturl = parsedurl.scheme + "://" + parsedurl.netloc + action

# Performs the submission of the IdP login form with the above post data
response = session.post(
    idpauthformsubmiturl, data=payload, verify=sslverification)

# Debug the response if needed
Duo = BeautifulSoup(response.text.decode('utf8'), "html5lib")

#print Duo

all_scripts = Duo.find_all('iframe')

# Locate the right script
duoscript = '' 

for script in Duo.find_all('iframe'):
    script = str(script)
    if "duosecurity.com" in script:
	duoscript = script 

# If string returns empty then display invalid username or password 
if (duoscript == ''):
    print 'Invalid username or password. Please try again...\n'
    sys.exit(0)

#print duoscript

jsonData = re.findall('"([^"]*)"', duoscript)
duohost =  jsonData[0]

# Get first part of sig: TX
#duotx = duoiframe['sig_request']
duotx = jsonData[2]
duotx = duotx.rsplit(':', 1)[0]

# Get second part of sig: APP
duoapp = jsonData[2]
duoapp = duoapp.rsplit(':',1)[1]

duoredirect = response.url

duoiframeurl = 'https://' + duohost + '/frame/web/v1/auth?tx=' + duotx + '&parent=' + duoredirect

# print duoiframeurl
duo_response = session.post(duoiframeurl)

# Get SID from the redirect url
getsid = duo_response.url
getsid = getsid.rsplit('=',1)[1]
getsid = urllib.unquote(getsid).decode('utf8')

formsoupduo = BeautifulSoup(duo_response.text.decode('utf8'), "html5lib")

# Prepare payload
duopayload = {}

for inputtag in formsoupduo.find_all(re.compile('(INPUT|input|select|option)')):
    name = inputtag.get('name','')
    value = inputtag.get('value','')
    if "sid" in name.lower():
	duopayload[name] = value
    elif "url" in name.lower():
	duopayload[name] = value
    elif "itype" in name.lower():
	duopayload[name] = value
    elif "days_to_block" in name.lower():
	duopayload[name] = value
    elif "device" in name.lower():
	duopayload[name] = 'phone1'
    elif "factor" in name.lower():
	if (authmethod == 'push'):
		duopayload[name] = 'Duo Push'
	if duo is True:
		duopayload[name] = 'Passcode'
	if (authmethod == 'call'):
		duopayload[name] = 'Phone Call'
    elif "passcode" in name.lower():
	duopayload[name] = authmethod
    elif "has-token" in name.lower():
	if duo is True:
		duopayload[name] = 'true'

for inputtag in formsoupduo.find_all(re.compile('(FORM|form)')):
    action = inputtag.get('action')
    if action:
        parsedurl = urlparse(duoiframeurl)
        duoformsubmiturl = parsedurl.scheme + "://" + parsedurl.netloc + action
# Performs the submission of the IdP login form with the above post data
duoresponse = session.post(
    duoformsubmiturl, data=duopayload, verify=sslverification)

json_a = json.loads(duoresponse.text)

# Check MFA method
if(json_a['stat'] == 'FAIL'): 
   print 'Invalid Authentication method. Please try agin...\n'
   sys.exit(0)

txid = json_a['response']['txid']
duo_url = 'https://' + duohost + '/frame/status'
duo_payload = {
	'sid': getsid,
	'txid': txid
}

# 
if duo is False:
  for i in xrange(5,0,-1):
       	time.sleep(1)
	if (i<=4):
    	    finalduoresponse = session.post(
		duo_url, data=duo_payload, verify=sslverification) 
    	    json_b = json.loads(finalduoresponse.text)
	    status = json_b['response']['status']
	    status_code = json_b['response']['status_code']
	    print status
      	    if ((status_code == 'allow')):
       		break

if duo is True:
   finalduoresponse = session.post(
       duo_url, data=duo_payload, verify=sslverification)
   json_b = json.loads(finalduoresponse.text)

if (json_b['response']['result'] == 'FAILURE'):
    print '\n'
    sys.exit(0)

getiFrameUrl = json_b['response']['result_url']

# Prepare DUO iFrame 
iFrameUrl = 'https://' + duohost + getiFrameUrl
statusPayload = {
	'sid': getsid
}

s_response = session.post(iFrameUrl, data=statusPayload, verify=sslverification)

json_c = json.loads(s_response.text)

final_url = json_c['response']['parent']
getcookie = json_c['response']['cookie'] +':'+ duoapp

final_payload = {
	'_eventId': 'proceed',
	'sig_response': getcookie
}

response = session.post(
      final_url, data=final_payload, verify=sslverification)

# Overwrite and delete the credential variables, just for safety
#username = '##############################################'
password = '##############################################'
#del username
del password

# Decode the response and extract the SAML assertion
soup = BeautifulSoup(response.text.decode('utf8'), "html5lib")
assertion = ''

# Look for the SAMLResponse attribute of the input tag (determined by
# analyzing the debug print lines above)
for inputtag in soup.find_all('input'):
    if(inputtag.get('name') == 'SAMLResponse'):
        #print(inputtag.get('value'))
        assertion = inputtag.get('value')

# Better error handling is required for production use.
if (assertion == ''):
    #TODO: Insert valid error checking/handling
    print 'Response did not contain a valid SAML assertion'
    sys.exit(0)

# Debug only - SAML Response
#print(base64.b64decode(assertion))

# Parse the returned assertion and extract the authorized roles
awsroles = []
root = ET.fromstring(base64.b64decode(assertion))
for saml2attribute in root.iter('{urn:oasis:names:tc:SAML:2.0:assertion}Attribute'):
    if (saml2attribute.get('Name') == 'https://aws.amazon.com/SAML/Attributes/Role'):
        for saml2attributevalue in saml2attribute.iter('{urn:oasis:names:tc:SAML:2.0:assertion}AttributeValue'):
            awsroles.append(saml2attributevalue.text)

# Note the format of the attribute value should be role_arn,principal_arn
# but lots of blogs list it as principal_arn,role_arn so let's reverse
# them if needed
for awsrole in awsroles:
    chunks = awsrole.split(',')
    if'saml-provider' in chunks[0]:
        newawsrole = chunks[1] + ',' + chunks[0]
        index = awsroles.index(awsrole)
        awsroles.insert(index, newawsrole)
        awsroles.remove(awsrole)

# If I have more than one role, ask the user which one they want,
# otherwise just proceed
print ""
if len(awsroles) > 1:
    i = 0
    print "Please choose the role you would like to assume:"
    for awsrole in awsroles:
        print '[', i, ']: ', awsrole.split(',')[0]
        i += 1
    print "\nSelection: ",
    selectedroleindex = raw_input()

    # Basic sanity check of input
    if int(selectedroleindex) > (len(awsroles) - 1):
        print 'You selected an invalid role index, please try again'
        sys.exit(0)

    role_arn = awsroles[int(selectedroleindex)].split(',')[0]
    principal_arn = awsroles[int(selectedroleindex)].split(',')[1]
else:
    role_arn = awsroles[0].split(',')[0]
    principal_arn = awsroles[0].split(',')[1]

#all_region = boto.sts.regions()
#print all_region

# Use the assertion to get an AWS STS token using Assume Role with SAML
conn = boto3.client('sts')

response = conn.assume_role_with_saml(RoleArn=role_arn, PrincipalArn=principal_arn, SAMLAssertion=assertion, DurationSeconds=tokenexpiration)

token = response["Credentials"]


# Write the AWS STS token into the AWS credential file
home = expanduser("~")
filename = home + awsconfigfile

# Read in the existing config file
config = ConfigParser.RawConfigParser()
config.read(filename)

# Put the credentials into a saml specific section instead of clobbering
# the default credentials
if not config.has_section('saml'):
    config.add_section('saml')

config.set('saml', 'output', outputformat)
config.set('saml', 'region', region)
config.set('saml', 'aws_access_key_id', token["AccessKeyId"])
config.set('saml', 'aws_secret_access_key', token["SecretAccessKey"])
config.set('saml', 'aws_session_token', token["SessionToken"])

# Write the updated config file
with open(filename, 'w+') as configfile:
    config.write(configfile)


#
local_tz = tzlocal.get_localzone()
expires = token['Expiration'].astimezone(local_tz)
#print("\nCredentials will expire {}".format(expires.isoformat()))

# Give the user some basic info as to what has just happened
print '\n\n----------------------------------------------------------------------------------------------------------------'
print '* Welcome ' + username + ','
print '* Your new access key pair has been stored in the AWS configuration file {0} under the saml profile.'.format(filename)
print '* Note that it will expire at {0}.'.format(expires)
print '* After this time, you may safely rerun this script to refresh your access key pair.' 
print '* To use this credential, call the AWS CLI with the --profile option'
print '* Example Cli\'s:'
print '* aws --profile saml s3api list-buckets'
print '* aws --profile saml ec2 describe-instances'
print '* aws --profile saml ec2 describe-vpcs'
print '--------------------------------------------------------------------------------------------------------------------\n\n'
